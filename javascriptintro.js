function changeText(){
    let text=document.getElementById("text");
    text.innerHTML="text changed";
    text.style.color="green";
}

//Array, Object
//A collection of multiple items under a single variable name
 
let batches = ["batch48","batch50","batch62", 134,true,"3445",986565.9];

for(let i =0; i<batches.length; i++){
    console.log(batches[i])
}

//Object data stored in key value pairs

let  person = {
    firstName: "John",
    lastName: "Doe",
    batchno: 62,
    qualification: "b-tech",
    skills: ["html","css","js"]
  }
  console.log(person.skills)
  console.log(person['firstName'])
  console.log(person.skills[2])

  //Predefined math functions

  let minvalue=Math.min(23,4,5,67,43,54);
  console.log(minvalue)

  let maxvalue=Math.max(454,987,77,8,45);
  console.log(maxvalue)

  let sqrt=Math.sqrt(4)
  console.log(sqrt);
  console.log(Math.pow(3,3));
  console.log(Math.floor(2.33));
  console.log(Math.ceil(2.33));

  //write a program to find the largest element in an array using math.max
 
  function minNum(array){
    
    return Math.min(...array)

  }
    
  console.log(minNum([8,10,20,30,40,90]));
  //WAP to find the largest element in an array  without using math.max

  //WAP to find sqrt
  function sqroot(num){
    return Math.sqrt(num)
  }
   console.log(sqroot(10));

   //WAP to display only 3 digits after decimal

   console.log(sqroot(2).toFixed(3))

   //WAP power

   function power(num1,num2){
    return Math.pow(num1,num2)
   }
   console.log(power(4,3))

   //WAP to find largest number in an array without using math.max

   function findLargestNumber(myarray){
    let max=0;
    for(let i=0; i<myarray.length;i++){
        if(myarray[i]>max){
            max=myarray[i];
        }
    }
       return max;
   }
   console.log(findLargestNumber([10,98,877,45,32]));

   // wap to find the second largest element in an array
function secondLarge(arr)
{
let max1=arr[0];
let max2= arr[0];
for(let i=0;i<arr.length;i++) {
if(arr[i]>max1)

    max2=max1;
    max1=arr[i];
}
 else if(arr[i]>max2 && arr[i]!=max1) {
    max2=arr[i];
}
return max2;
}
console.log(secondLarge([4,7,12,20,29]));

//WAP to smallest element in a given array
function findSmallestNumber(myarray){
  let min=0;
  for (let i = 0; i < myarray; i++) {
    if (temp[i] < value) {
      value = temp[i];
      index = i;
      }
  }
     return min;
 }
 console.log(findSmallestNumber([10,98,877,45,32]));

// wap to find the  2nd smallest number
function secondSmall(array)
{
    let small1=array[0];
    let small2=Infinity;
    for(let i=0;i<array.length;i++)
    {
        if(array[i]<small1)
        {
            small2=small1;
            small1=array[i];
        }
        else if(array[i]>small1 && array[i]<small2)
        {
            small2=array[2];
        }
    }
    return small2;
}
console.log(secondSmall([5,1,3,6,7,9]));



//WAP for fibanacci series

   // Program to generate Fibonacci series up to n terms
// Take input from the user
const number = parseInt(prompt('Enter the number of terms: '));
let n1 = 0, n2 = 1, nextTerm;

console.log('Fibonacci Series:');
for (let i = 1; i <= number; i++) {
    console.log(n1);
    nextTerm = n1 + n2;
    n1 = n2;
    n2 = nextTerm;
}


//String methods
let a="hello world"
let c=a.trim();
console.log(c);



let text1="Talent";
let text2="Sprint";
console.log(tex1.concat(text2));

