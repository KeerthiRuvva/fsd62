//1.printing even numbers
function printeven(n) {
    var str = ""
    let i=0
    while(i <= n) {
        str += i + " "
        i+=2
    }
    return str
}
console.log(printeven(100))

//2.sum of even numbers
function evensum(n){
    let esum=0
    let i=0
    while(i<=n){
        esum+=i
        i+=2
    }
    return esum
}
console.log(evensum(12))

//3.sum of odd numbers
function oddsum(n){
    let osum=0
    let i=1
    while(i<=n){
        osum+=i
        i+=2
    }
    return osum    
}
console.log(oddsum(5))



//4.printing even numbers
function printodd(n) {
    var str = ""
    let i=1
    while(i <= n) {
        str += i + " "
        i+=2
    }
    return str
}
console.log(printodd(5))

//5.count the vowels
function countvowels(arr) {
    var count = 0;
    var i = 0
    vowels = ['a', 'e', 'i', 'o', 'u']
    while (i < arr.length) {
        i++
        if (vowels.includes(arr.charAt(i).toLowerCase())) {
            count++
        }
    }
    return count;
}
console.log(countvowels("jhdifiefgkjsdbfyg"))

//6.sum of array
function sum(arr) {
    var sum = 0;
    let i=0
    while(i < arr.length) {
        sum += arr[i]
        i++
    }
    return sum
}
console.log(sum([25,85,74,69]))
//7.largest array
function largestinarray(arr) {
    var maxi = 0;
    let i=0
    while(i < arr.length - 1) {
        if (arr[i] > maxi) {
            maxi = arr[i]
        }
        i++
    }
    return maxi
}
console.log(largestinarray([74,21,96,55]))
//8.smallest
let a = [20, 30, 67, 24, 73]
let min = a[0];
let i=0
while ( i < a.length) {
    if (a[i] < min) {
        min = a[i]; //max=20; 30;60
    }
    i++
}
console.log(min);
//9..second largest
function secondlargest(arr) {
    let max1 = arr[0];
    let max2 = arr[0];
    let i=0
    while(i < arr.length){
        if (arr[i] > max1) {
            max2 = max1;
            max1 = arr[i];
        }
        else if (arr[i] > max2 && arr[i] != max1) {
            max2 = arr[i];
        }
        i++
    }
    return max2;
}
console.log(secondlargest([85,45,73,91]))
//10.second smallest
function secondsmallest(arr) {
    let min1 = arr[0];
    let min2 = Infinity;
    let i=0
    while(i < arr.length){
        if (arr[i] < min1) {
            min2 = min1;
            min1 = arr[i];
        }
        else if (arr[i] > min1 && arr[i] < min2) {
            min2 = arr[i];
        }
        i++
    }
    return min2;
}
console.log(secondsmallest([59,45,12,85]))

//11.reversing the digits
function reversedigits(num){
    let sum=""
    while(num>0){
        sum+=num%10
        num=Math.floor(num/10)
    }
    return sum
}
console.log(reversedigits(964))

//other method
let num=345
let reverseNum =0 
    while(num!=0) {
        let lastnum=num%10;
        reverseNum=(reverseNum*10)+lastnum;
        num=Math.floor(num/10)
}
console.log(reverseNum)

//12.sum of digits
function sumofdigits(num){
    let sum=0
    while(num>0){
        sum+=num%10
        num=Math.floor(num/10)
    }
    return sum
}
console.log(sumofdigits(234))
//13..average of an array
function avg(arr){
    let sum=0
    let i=0
    while(i<arr.length){
        sum+=arr[i]
        i++
    }
    let avg=sum/arr.length
    return avg
}
console.log(avg([1,85,96,75,6]))

//pattern

function squarePattern(num) {
    for(let i=0; i<num; i++){
    let line= " "
    for(let j=0; j<num; j++){
        line+="*"
        console.log(line)
       }
       
    }
    
}
 (squarePattern(5));

 //pattern

 function squarePattern(num) {
    for(let i=0; i<num; i++){
    let line= " "
    for(let j=0; j<num; j++){
        line+="*"
        console.log(line)
       }
       
    }
}
 (squarePattern(5));


 //WAP to find square root of given number without using math functions
 // input 15
 //output 3

 //input 25
 //output 5

 //input 48
 //output 6

 //*,-,/,%

 function squareroot(num) {
    for(let i=1;i<=num/2;i++) {

    
       if(i*i>num){
        return i-1
       }  
       if(i*i==num) {
        return i
       }
    
}
    }
    console.log(squareroot(48));


    //WAP to print the even indexed  values of a given string
    //input : sai charan
    //output: sihrn

    function evenind(arr){
        let str="";
        let i=0 ;
        while(i<arr.length){
            str+=arr[i]
            i+=2
        }
        return str
    }
    console.log(evenind("saicharan"))

    //other

    function evenind(str) {
        let strInd="";
        let i=0;
        while(i<str.length) {
            if(i%2==0) {
                strInd+=str.charAt(i)
            }
            i++
        }
        return strInd;
    }
      console.log(evenind("saicharan"));

      //concat two strings
      //alternative string concatination
      //iput: str1='abc', str2='pqr'
      //output: apbqcr

      //input2: str1='mnopq', str2='ijklmn'
      //output: minjokplqmn

      function stringJoin (n,s) {
        let s1= s.length
        let n1= n.length
        let str="";
        let max= Math.max(s.length,n.length);
        for (let i=0;i<max;i++) {
            str +=n.charAt(i)
        }

      }

      function pattern(n){
        let str=""
        for(let i=0;i<n;i++){
            for(let j=0;j<n;j++){
                if(i==0 || j==0 || i==n-1 || j==n-1){
                    str+="*"
                }else{
                    str+=""
                }
            }
            str+="\n"
        }
        return str
    }
    console.log(pattern(5))


    function squarePattern(num) {
        for(let i=0; i<num; i++){
        let line= "  "
        for(let j=0; j<num; j++){
            line+="* "
            console.log(line)
           }
           
        }
    }
     (squarePattern(5));


     function butterfly(n){
        let str=""
        for(let i=0;i<n;i++){
            for(let j=0;j<n;j++){
                if( j==0 || i==j || j==n-1 || i+j==n-1) {
                    document.write("*  ")
                }else{
                    document.write("&nbsp;&nbsp;&nbsp;")
                }
            }
            document.write("<br>")
        }
        return str
    }
    console.log(butterfly(5))

    

//     for(leti=1;i<4;i++) {
//     let row =""
//     for (let j-0;j<4;j++) {
//     row++
//     }
//     console.log()
// }